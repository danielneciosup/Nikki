package com.unmsm.gps.app.danielneciosup.nikki.data.source;

import android.support.annotation.NonNull;

import com.unmsm.gps.app.danielneciosup.nikki.bean.Alarm;
import com.unmsm.gps.app.danielneciosup.nikki.bean.Note;

import java.util.List;

/**
 * Created by Daniel on 28/10/2016.
 */
public interface NotesDataSource
{
    interface LoadNotesCallback
    {
        void onTaskLoaded(List<Note> notes);
        void onDataNotAvailable();
    }

    interface GetNoteCallback
    {
        void onNoteLoaded( Note note );
        void onDataNotAvailable();
    }

    interface InsertNoteCallback{
        void onNoteInserted();
        void onNoteNotInserted();
    }

    void getNotesByUserId( @NonNull LoadNotesCallback callback, Integer userId );
    void getNote( @NonNull int noteId, @NonNull GetNoteCallback callback );
    void addAlarm( @NonNull int noteId, @NonNull Alarm alarm );
    void createNote(@NonNull InsertNoteCallback callback, @NonNull Note note );
    void getNotesByCategoryId(@NonNull LoadNotesCallback callback, @NonNull int categoryId );
}
