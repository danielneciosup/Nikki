package com.unmsm.gps.app.danielneciosup.nikki.data.repository;

import android.support.annotation.NonNull;

import com.unmsm.gps.app.danielneciosup.nikki.data.source.UsersDataSource;

/**
 * Created by DINJO-PC on 23/11/2016.
 */
public class UsersRepository implements UsersDataSource {

    private static UsersRepository INSTANCE = null;

    private final UsersDataSource mUsersRemoteDataSource;
    private final UsersDataSource mUsersLocalDataSource;

    private UsersRepository(UsersDataSource mUsersRemoteDataSource, UsersDataSource mUsersLocalDataSource) {
        this.mUsersRemoteDataSource = mUsersRemoteDataSource;
        this.mUsersLocalDataSource = mUsersLocalDataSource;
    }

    public static UsersRepository getInstance(UsersDataSource mUsersRemoteDataSource, UsersDataSource mUsersLocalDataSource) {
        if( INSTANCE == null ){
            INSTANCE = new UsersRepository(mUsersRemoteDataSource,mUsersLocalDataSource);
        }
        return INSTANCE;
    }

    @Override
    public void getUserByEmail(@NonNull GetUserCallback callback, @NonNull String email) {
        mUsersLocalDataSource.getUserByEmail(callback,email);
    }

}
