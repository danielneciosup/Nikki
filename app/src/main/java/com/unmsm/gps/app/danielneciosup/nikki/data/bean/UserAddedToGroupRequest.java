package com.unmsm.gps.app.danielneciosup.nikki.data.bean;

/**
 * Created by DINJO-PC on 20/11/2016.
 */
public class UserAddedToGroupRequest
{
    private String email;
    private String groupid;

    public UserAddedToGroupRequest(String email, String groupid)
    {
        this.email = email;
        this.groupid = groupid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }
}
