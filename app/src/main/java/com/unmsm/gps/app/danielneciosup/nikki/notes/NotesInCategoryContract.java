package com.unmsm.gps.app.danielneciosup.nikki.notes;

import com.unmsm.gps.app.danielneciosup.nikki.adapter.NoteAdapter;
import com.unmsm.gps.app.danielneciosup.nikki.bean.Note;
import com.unmsm.gps.app.danielneciosup.nikki.util.BaseView;
import com.unmsm.gps.app.danielneciosup.nikki.util.RefreshableContract;

/**
 * Created by DINJO-PC on 24/11/2016.
 */
public interface NotesInCategoryContract {

        public interface View extends BaseView<Presenter> {

        void displaySomeMessage(String message);

        void showNoteDetail(int noteId);

        void backToCategories();

        void setRecyclerView();

        void setCategoryName(String categoryName);

        String getCategoryName();

        void setNoteAdapter(NoteAdapter adapter);

        Presenter getPresenter();

        void setParentView(MainNotesContract.View parentView);

    }

    public interface Presenter extends RefreshableContract.Presenter<Note>{

        void onListNotes();

        void onNoteClicked(Note note);

        void onBackToCategoriesClick();

    }

}
