package com.unmsm.gps.app.danielneciosup.nikki.notes;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.unmsm.gps.app.danielneciosup.nikki.adapter.CategoryAdapter;
import com.unmsm.gps.app.danielneciosup.nikki.bean.Category;
import com.unmsm.gps.app.danielneciosup.nikki.bean.Note;
import com.unmsm.gps.app.danielneciosup.nikki.data.repository.CategoryRepository;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.CategoriesDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.local.CategoryLocalDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.util.NikkiUtils;
import com.unmsm.gps.app.danielneciosup.nikki.util.SessionManager;

import java.util.List;

/**
 * Created by Daniel on 16/10/2016.
 */
public class MyNotesPresenter implements MyNotesContract.Presenter
{
    MyNotesContract.View mView;
    CategoryRepository categoryRepository;
    SessionManager sessionManager;

    public MyNotesPresenter(MyNotesContract.View view, Context context)
    {
        mView = view;

        categoryRepository = CategoryRepository.getInstance(
                null,
                CategoryLocalDataSource.getInstance(context)
        );

        sessionManager = new SessionManager(context);
    }

    @Override
    public void onCategorySelected(Category category) {
        sessionManager.setSomeEntity(NikkiUtils.NOTE_VIEW_KEY,NotesInCategoryFragment.class.getCanonicalName(),String.class);
        sessionManager.setSomeEntity(NikkiUtils.CATEGORY_SELECTED_KEY,category.getTxtNombre(),String.class);
        mView.showNotesInCategorySelected();

    }

    @Override
    public void onListCategories() {
        categoryRepository.getCategories(new CategoriesDataSource.LoadCategoriesCallback() {
            @Override
            public void onCategoriesLoaded(List<Category> categories) {
                CategoryAdapter categoryAdapter = new CategoryAdapter(mView.getPresenter(),categories);
                mView.setCategoryAdapter(categoryAdapter);
            }

            @Override
            public void onDataNotAvailable() {
                mView.displaySomeMessage("Error cargando categorías :(");
            }
        });
    }

    @Override
    public void onItemAdd(Note item) {

    }

    @Override
    public void onItemDelete(Note item) {

    }
}
