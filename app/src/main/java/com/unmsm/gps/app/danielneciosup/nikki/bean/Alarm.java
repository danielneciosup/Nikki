package com.unmsm.gps.app.danielneciosup.nikki.bean;

/**
 * Created by Daniel on 16/10/2016.
 */
public class Alarm
{
    private int idAlarma;
    private String hrAlarm;
    private int idUser;
    private int idNote;


    public Alarm(int idAlarma, String hrAlarm, int idUser, int idNote) {
        this.idAlarma = idAlarma;
        this.hrAlarm = hrAlarm;
        this.idUser = idUser;
        this.idNote = idNote;
    }

    public int getIdAlarma()
    {
        return idAlarma;
    }

    public void setIdAlarma(int idAlarma) {
        this.idAlarma = idAlarma;
    }

    public String getHrAlarm() {
        return hrAlarm;
    }

    public void setHrAlarm(String hrAlarm) {
        this.hrAlarm = hrAlarm;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdNote() {
        return idNote;
    }

    public void setIdNote(int idNote) {
        this.idNote = idNote;
    }
}
