package com.unmsm.gps.app.danielneciosup.nikki.data.bean;

/**
 * Created by DINJO-PC on 19/11/2016.
 */
public class GroupResponse
{
    private String name;
    private Boolean  isadmin;
    private Integer quantity;
    private String groupid;

    public GroupResponse(String name, Boolean isadmin, Integer quantity, String groupid) {
        this.name = name;
        this.isadmin = isadmin;
        this.quantity = quantity;
        this.groupid = groupid;
    }

    public Boolean getIsadmin()
    {
        return isadmin;
    }

    public void setIsadmin(Boolean isadmin)
    {
        this.isadmin = isadmin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }
}
