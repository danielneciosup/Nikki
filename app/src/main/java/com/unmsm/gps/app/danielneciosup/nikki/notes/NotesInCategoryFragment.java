package com.unmsm.gps.app.danielneciosup.nikki.notes;

import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.unmsm.gps.app.danielneciosup.nikki.R;
import com.unmsm.gps.app.danielneciosup.nikki.adapter.CategoryAdapter;
import com.unmsm.gps.app.danielneciosup.nikki.adapter.NoteAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by DINJO-PC on 24/11/2016.
 */
public class NotesInCategoryFragment extends Fragment implements NotesInCategoryContract.View{

    private NotesInCategoryContract.Presenter presenter;
    private MainNotesContract.View parentView;
    @BindView(R.id.recview_notes)RecyclerView rcvNotes;
    @BindView(R.id.title_notes)TextView tvNotes;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View mView = inflater.inflate( R.layout.fragment_notes_in_category, container, false );

        ButterKnife.bind(this,mView);

        setPresenter(new NotesInCategoryPresenter(this,getContext()));

        setRecyclerView();

        presenter.onListNotes();

        parentView.setRefresheablePresenterOfActiveView(presenter);

        return mView;
    }

    @OnClick(R.id.btn_back_categories)
    public void OnBack(){
        presenter.onBackToCategoriesClick();
    }

    @OnClick(R.id.title_notes)
    public void OnReturnToCategories(){
        presenter.onBackToCategoriesClick();
    }

    @Override
    public void backToCategories(){
        parentView.showCategoriesView();
    }

    @Override
    public void displaySomeMessage(String message) {
        parentView.showSomeMessage(message);
    }

    @Override
    public void showNoteDetail(int noteId) {
        parentView.showSomeMessage(Integer.toString(noteId));
    }

    @Override
    public void setRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rcvNotes.setLayoutManager( layoutManager );
        rcvNotes.setHasFixedSize(true);
    }

    @Override
    public void setCategoryName(String categoryName) {
        tvNotes.setText("Categorías \\ "+categoryName+" \\ ");
    }

    @Override
    public String getCategoryName() {
        return tvNotes.getText().toString();
    }

    @Override
    public void setNoteAdapter(NoteAdapter adapter) {
        rcvNotes.setAdapter(adapter);
    }

    @Override
    public NotesInCategoryContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void setParentView(MainNotesContract.View parentView) {
        this.parentView = parentView;
    }

    @Override
    public void setPresenter(NotesInCategoryContract.Presenter presenter) {
        this.presenter = presenter;
    }

}
