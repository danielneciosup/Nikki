package com.unmsm.gps.app.danielneciosup.nikki.data.bean;

/**
 * Created by DINJO-PC on 20/11/2016.
 */
public class UserAddedToGroupResponse
{
    private String email;

    public UserAddedToGroupResponse(String email)
    {
        this.email = email;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }
}
