package com.unmsm.gps.app.danielneciosup.nikki.util;

/**
 * Created by Daniel on 25/11/2016.
 */
public interface BaseCallback
{
    void onDataNotAvailable();
}
