package com.unmsm.gps.app.danielneciosup.nikki.group;

import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateResponse;
import com.unmsm.gps.app.danielneciosup.nikki.util.BaseView;

/**
 * Created by DINJO-PC on 12/11/2016.
 */
public interface CreateGroupContract
{
    interface Presenter
    {
        void onCreateGroup(String nameGroup);
        void onCreateSuccessGroup(GroupCreateResponse groupCreateResponse);
        void onAddMembersToGroup(GroupCreateResponse groupCreateResponse);
        void onCloseCreateGroup();
    }

    interface View extends BaseView<Presenter>
    {
        void displayCreateSuccess(GroupCreateResponse groupCreateResponse);
        void displayCancelCreateGroup();
        void displayCreateError();
        void displayErrorConnection();
        void closeCreateGroup();

        void displayAddMembersToGroup(GroupCreateResponse groupCreateResponse);
    }
}
