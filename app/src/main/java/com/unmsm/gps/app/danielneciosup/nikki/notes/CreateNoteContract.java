package com.unmsm.gps.app.danielneciosup.nikki.notes;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.unmsm.gps.app.danielneciosup.nikki.home.HomeContract;
import com.unmsm.gps.app.danielneciosup.nikki.util.BaseView;
import com.unmsm.gps.app.danielneciosup.nikki.util.RefreshableContract;

/**
 * Created by Daniel on 16/10/2016.
 */
public interface CreateNoteContract
{

    interface Presenter extends DatePickerDialog.OnDateSetListener,
    TimePickerDialog.OnTimeSetListener{
        void onCreate();
        void onCancel(Dialog dialog);
        void onDateEdit();
        void onTimeEdit();
    }

    interface View extends BaseView< Presenter >
    {
        void displayCancel();
        void displayNoteCreated();
        void displaySomeMessage(String errorMessage);
        void showDatePicker();
        void showTimePicker();
        void setSelectedDate(String date);
        void setSelectedTime(String time);
        void hide();
        String getNoteTitle();
        String getNoteDate();
        String getNoteTime();
        void setRefresheableNotePresenter(RefreshableContract.Presenter presenter);
        RefreshableContract.Presenter getRefresheableNotePresenter();
        void setParentPresenter(MainNotesContract.Presenter presenter);
        MainNotesContract.Presenter getParentPresenter();
    }
}
