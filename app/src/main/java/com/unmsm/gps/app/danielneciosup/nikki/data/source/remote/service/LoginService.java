package com.unmsm.gps.app.danielneciosup.nikki.data.source.remote.service;

import com.unmsm.gps.app.danielneciosup.nikki.data.bean.AccessTokenRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.AccessTokenResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by DINJO-PC on 13/11/2016.
 */
public interface LoginService
{
    @POST( "users/login" )
    Call<AccessTokenResponse> sendCredentialsForLogin(@Body AccessTokenRequest request);
}
