package com.unmsm.gps.app.danielneciosup.nikki.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.unmsm.gps.app.danielneciosup.nikki.R;
import com.unmsm.gps.app.danielneciosup.nikki.bean.Note;
import com.unmsm.gps.app.danielneciosup.nikki.notes.NotesInCategoryContract;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Daniel on 31/10/2016.
 */
public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.ViewHolderNote>
{
    List<Note> noteList;
    NotesInCategoryContract.Presenter presenter;

    public NoteAdapter(List<Note> noteList, NotesInCategoryContract.Presenter presenter)
    {
        this.noteList = noteList;
        this.presenter = presenter;
    }

    public List<Note> getNoteList() {
        return noteList;
    }

    public class ViewHolderNote extends RecyclerView.ViewHolder
    {
        @BindView( R.id.txtview_note )
        TextView txtviewNote;
        @BindView( R.id.txtview_nom_group )
        TextView txtviewNomGroup;
        @BindView( R.id.txtview_num_alarms )
        TextView txtviewNumAlarms;
        @BindView( R.id.imgview_group )
        ImageView imgviewGroup;
        @BindView(R.id.note_cardview)
        LinearLayout cardviewNote;

        public ViewHolderNote(View itemView)
        {
            super(itemView);
            ButterKnife.bind( this, itemView );
        }
    }

    @Override
    public ViewHolderNote onCreateViewHolder( ViewGroup parent, int viewType )
    {
        View holder = LayoutInflater.from( parent.getContext() ).inflate( R.layout.cardview_note, parent, false );

        return new ViewHolderNote( holder );
    }

    @Override
    public void onBindViewHolder(ViewHolderNote holder, final int position)
    {

        holder.txtviewNote.setText( noteList.get( position ).getTxDescrip() );
        holder.txtviewNomGroup.setText( noteList.get( position ).getNomGroup() );
        //holder.txtviewNumAlarms.setText( noteList.get( position ).getNumAlarms() );

        holder.cardviewNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onNoteClicked(noteList.get(position));
            }
        });

        if( noteList.get( position ).getNomGroup() == null ) holder.imgviewGroup.setVisibility( View.INVISIBLE );
    }

    @Override
    public int getItemCount()
    {
        return noteList.size();
    }
}
