package com.unmsm.gps.app.danielneciosup.nikki.data.source.local;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.unmsm.gps.app.danielneciosup.nikki.bean.User;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.UsersDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.local.sqlite.NikkiDbContract;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.local.sqlite.NikkiDbHelper;

/**
 * Created by DINJO-PC on 23/11/2016.
 */
public class UsersLocalDataSource implements UsersDataSource {

    private static UsersLocalDataSource INSTANCE;

    private NikkiDbHelper mNikkiDbHelper;

    private UsersLocalDataSource(@NonNull Context context)
    {
        mNikkiDbHelper = NikkiDbHelper.getInstance( context );
    }

    public static UsersLocalDataSource getInstance( @NonNull Context context )
    {
        if ( INSTANCE == null )
            INSTANCE = new UsersLocalDataSource( context.getApplicationContext() );

        return INSTANCE;
    }

    @Override
    public void getUserByEmail(@NonNull GetUserCallback callback, @NonNull String email) {
        SQLiteDatabase db = mNikkiDbHelper.getReadableDatabase();
        try {
            String query = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1;",
                    NikkiDbContract.UserEntry.TABLE_NAME,
                    NikkiDbContract.UserEntry.TX_EMAIL);
            final Cursor cursor = db.rawQuery(query, new String[]{email});
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    User user = new User();
                    user.setIdUser(cursor.getInt(cursor.getColumnIndex(NikkiDbContract.UserEntry._ID)));
                    user.setTxtEmail(cursor.getString(cursor.getColumnIndex(NikkiDbContract.UserEntry.TX_EMAIL)));
                    callback.onLoadedUser(user);
                } else {
                    callback.onDataNotAvailable();
                }
            } else {
                callback.onDataNotAvailable();
            }
        }catch (Exception ex){
            callback.onDataNotAvailable();
        }
    }

}
