package com.unmsm.gps.app.danielneciosup.nikki.group;

import android.content.Context;

import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupResponse;
import com.unmsm.gps.app.danielneciosup.nikki.util.BaseView;

import org.json.JSONException;

import java.util.List;

/**
 * Created by Daniel on 25/11/2016.
 */
public interface MyGroupsContract
{
    interface Presenter
    {
        void onLoadGroups() throws JSONException;

        void onDeleteNote( GroupResponse groupResponse );
    }

    interface View extends BaseView<Presenter>
    {
        void showGroups( List<GroupResponse> listGroup );

        void showMessageGroupsNotAvailable();

        void showMessageGroupDelete();

        void showMessageErrorServer();

        void showMessageErrorConnection();
    }
}
