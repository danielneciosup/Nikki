package com.unmsm.gps.app.danielneciosup.nikki.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.unmsm.gps.app.danielneciosup.nikki.R;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by DINJO-PC on 13/11/2016.
 */
public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.ViewHolderMember>
{
    public interface OnItemOptionDeleteClickListener
    {
        void onItemDeleteOption( int position );
    }

    private static List<UserAddedToGroupResponse> usersAddeds;
    private OnItemOptionDeleteClickListener listener;

    public MemberAdapter( OnItemOptionDeleteClickListener listener )
    {
        this.usersAddeds = new ArrayList<>();
        this.listener = listener;
    }

    public class ViewHolderMember extends RecyclerView.ViewHolder
    {
        @BindView(R.id.txtview_email_member)
        TextView txtviewEmailMember;

        @BindView(R.id.btn_delete_member_added)
        Button btnDeleteMemberAdded;

        public ViewHolderMember(View itemView)
        {
            super(itemView);
            ButterKnife.bind( this, itemView );
        }

        public void bind(final UserAddedToGroupResponse item, final OnItemOptionDeleteClickListener listener, final int position )
        {
            txtviewEmailMember.setText( item.getEmail() );
            btnDeleteMemberAdded.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    listener.onItemDeleteOption( position );
                }
            });
        }
    }

    @Override
    public ViewHolderMember onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.cardview_email, parent, false );
        return new ViewHolderMember( view );
    }

    @Override
    public void onBindViewHolder(ViewHolderMember holder, int position)
    {
        holder.bind( usersAddeds.get( position ),listener, position);
    }

    @Override
    public int getItemCount()
    {
        return usersAddeds.size();
    }

    public static void add(UserAddedToGroupResponse response)
    {
        usersAddeds.add( response );
    }

    public static void delete( int posItem )
    {
        usersAddeds.remove( posItem );
    }

    public static List<UserAddedToGroupResponse> getUsersAddeds()
    {
        return usersAddeds;
    }
}
