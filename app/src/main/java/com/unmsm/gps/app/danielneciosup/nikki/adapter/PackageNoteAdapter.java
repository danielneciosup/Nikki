package com.unmsm.gps.app.danielneciosup.nikki.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.unmsm.gps.app.danielneciosup.nikki.R;
import com.unmsm.gps.app.danielneciosup.nikki.bean.Category;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Daniel on 17/10/2016.
 */
public class PackageNoteAdapter  extends RecyclerView.Adapter<PackageNoteAdapter.ViewHolderNote>
{
    List<Category> categoryList;

    public PackageNoteAdapter(List<Category> categoryList)
    {
        this.categoryList = categoryList;
    }

    public class ViewHolderNote extends RecyclerView.ViewHolder
    {
        @BindView( R.id.txtview_category) TextView txtviewCategory;

        public ViewHolderNote(View itemView)
        {
            super(itemView);
            ButterKnife.bind( this, itemView );
        }
    }

    @Override
    public ViewHolderNote onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View holder = LayoutInflater.from( parent.getContext() ).inflate( R.layout.cardview_category, parent, false );
        return new ViewHolderNote( holder );
    }

    @Override
    public void onBindViewHolder(ViewHolderNote holder, int position)
    {
        holder.txtviewCategory.setText( categoryList.get( position ).getTxtNombre() );
    }

    @Override
    public int getItemCount()
    {
        return categoryList.size();
    }


}
