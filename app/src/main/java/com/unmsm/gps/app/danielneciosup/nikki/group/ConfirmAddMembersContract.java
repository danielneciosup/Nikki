package com.unmsm.gps.app.danielneciosup.nikki.group;

import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateResponse;
import com.unmsm.gps.app.danielneciosup.nikki.util.BaseView;

/**
 * Created by DINJO-PC on 13/11/2016.
 */
public interface ConfirmAddMembersContract
{
    interface Presenter
    {
        void onConfirm(GroupCreateResponse groupCreateResponse);
        void onDeny();
        void onCloseConfirmation();
    }

    interface View extends BaseView<Presenter>
    {
        void displayConfirm();
        void displayDeny();
        void closeConfirmation();
    }
}
