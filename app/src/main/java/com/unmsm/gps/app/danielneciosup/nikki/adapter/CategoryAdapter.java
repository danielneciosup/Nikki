package com.unmsm.gps.app.danielneciosup.nikki.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unmsm.gps.app.danielneciosup.nikki.R;
import com.unmsm.gps.app.danielneciosup.nikki.bean.Category;
import com.unmsm.gps.app.danielneciosup.nikki.bean.Note;
import com.unmsm.gps.app.danielneciosup.nikki.notes.MyNotesContract;

import java.util.List;
import java.util.zip.Inflater;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by DINJO-PC on 24/11/2016.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolderCategory> {

    private MyNotesContract.Presenter mPresenter;
    private List<Category> categories;

    public CategoryAdapter(MyNotesContract.Presenter mPresenter, List<Category> categories) {
        this.mPresenter = mPresenter;
        this.categories = categories;
    }

    @Override
    public ViewHolderCategory onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_category,parent,false);
        return new ViewHolderCategory(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderCategory holder, int position) {
        final Category category = categories.get(position);
        holder.categoryText.setText(category.getTxtNombre());
        holder.categoryCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.onCategorySelected(category);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class ViewHolderCategory extends RecyclerView.ViewHolder{

        @BindView(R.id.txtview_category)TextView categoryText;
        @BindView(R.id.category_card_content)RelativeLayout categoryCard;

        public ViewHolderCategory(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

    }

}
