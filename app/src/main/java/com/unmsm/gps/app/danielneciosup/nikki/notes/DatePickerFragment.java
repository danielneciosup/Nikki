package com.unmsm.gps.app.danielneciosup.nikki.notes;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

/**
 * Created by Daniel on 03/11/2016.
 */
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener
{
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {

        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get( Calendar.YEAR );
        int month = calendar.get( Calendar.MONTH );
        int day = calendar.get( Calendar.DAY_OF_MONTH );

        return new DatePickerDialog( getActivity(),this, year, month, day );
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day)
    {

    }
}
