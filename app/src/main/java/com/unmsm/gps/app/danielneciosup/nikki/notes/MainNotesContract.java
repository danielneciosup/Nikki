package com.unmsm.gps.app.danielneciosup.nikki.notes;

import android.support.v4.app.Fragment;

import com.unmsm.gps.app.danielneciosup.nikki.util.BaseView;
import com.unmsm.gps.app.danielneciosup.nikki.util.RefreshableContract;

/**
 * Created by DINJO-PC on 24/11/2016.
 */
public interface MainNotesContract {

    public interface View extends BaseView<Presenter>{

        void showCategoriesView();
        void showNotesView();
        void showSomeMessage(String message);
        Presenter getPresenter();
        void setRefresheablePresenterOfActiveView(RefreshableContract.Presenter presenter);
        RefreshableContract.Presenter getRefresheablePresenterOfActiveView();
    }

    public interface Presenter{
        void onViewToShowRequired();
        void showNotification(String message);
    }
}
