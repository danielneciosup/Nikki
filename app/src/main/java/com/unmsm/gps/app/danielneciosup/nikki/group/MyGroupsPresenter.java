package com.unmsm.gps.app.danielneciosup.nikki.group;

import android.content.Context;

import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupResponse;
import com.unmsm.gps.app.danielneciosup.nikki.data.repository.GroupRepository;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.GroupsDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.local.GroupLocalDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.remote.GroupRemoteDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.util.SessionManager;

import org.json.JSONException;

import java.util.List;

/**
 * Created by Daniel on 25/11/2016.
 */
public class MyGroupsPresenter implements MyGroupsContract.Presenter
{
    private MyGroupsContract.View view;

    private GroupRepository groupRepository;

    private Context context;

    private SessionManager sessionManager;

    public MyGroupsPresenter(MyGroupsContract.View view, Context context)
    {
        this.view = view;
        this.context = context;
        groupRepository = GroupRepository.getInstance(GroupLocalDataSource.getInstance(), GroupRemoteDataSource.getInstance());
        sessionManager = new SessionManager( context );
    }

    @Override
    public void onLoadGroups()
    {
        try
        {
            String userid = sessionManager.getUserEntity().getUserid();

            groupRepository.getGroups( new GroupsDataSource.LoadedGroupsCallback()
            {
                @Override
                public void onGroupsLoaded(List<GroupResponse> groups)
                {
                    view.showGroups( groups );
                }

                @Override
                public void onDataNotAvailable()
                {
                    view.showMessageGroupsNotAvailable();
                }

                @Override
                public void onErrorServer()
                {
                    view.showMessageErrorServer();
                }

                @Override
                public void onErrorConnection()
                {
                    view.showMessageErrorConnection();
                }
            }, userid );
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onDeleteNote(GroupResponse groupResponse)
    {

    }
}
