package com.unmsm.gps.app.danielneciosup.nikki.group;

import com.unmsm.gps.app.danielneciosup.nikki.bean.User;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupResponse;
import com.unmsm.gps.app.danielneciosup.nikki.util.BaseView;

import java.util.List;

/**
 * Created by DINJO-PC on 12/11/2016.
 */
public interface AddMembersToGroupContract
{
    interface Presenter
    {
        void onAddMember(UserAddedToGroupRequest request);
        void onDeleteMember(int position);
        void onLoadAllUsersAdded();
    }

    interface View extends BaseView<Presenter>
    {
        void displayAddMember();
        void displayMemberAdded(UserAddedToGroupResponse response);
        void showMembersAdded();
        void displayMemberDelete(int position);
        void onResumeMembers();
        void showMessageUserNotFound();
        void showMessageMemberAlreadyExists();
        void showMessageErrorConnection();
    }
}
