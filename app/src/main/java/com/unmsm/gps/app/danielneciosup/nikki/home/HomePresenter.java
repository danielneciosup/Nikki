package com.unmsm.gps.app.danielneciosup.nikki.home;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.unmsm.gps.app.danielneciosup.nikki.group.CreateGroupDFragment;
import com.unmsm.gps.app.danielneciosup.nikki.util.RetrofitRemoteSingleton;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.AccessTokenResponse;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.AccessTokenRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.remote.service.CreateUserService;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.remote.service.LoginService;
import com.unmsm.gps.app.danielneciosup.nikki.util.NikkiUtils;
import com.unmsm.gps.app.danielneciosup.nikki.util.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Daniel on 11/09/2016.
 */
public class HomePresenter implements HomeContract.Presenter
{
    private HomeContract.View mView;

    private Retrofit retrofit;

    private CreateUserService createUserService;
    private LoginService loginService;
    private SessionManager sessionManager;
    private int currentPage;

    public HomePresenter(HomeContract.View mView, Context context)
    {
        this.mView = mView;
        retrofit = RetrofitRemoteSingleton.getRetrofit();
        createUserService = retrofit.create( CreateUserService.class );
        loginService = retrofit.create( LoginService.class );
        currentPage = 0;
        sessionManager = new SessionManager( context );
    }

    @Override
    public void onSendAccessToken()
    {
        Call<AccessTokenResponse> accessTokenCall = createUserService.sendCredentialsForCreateUser(new AccessTokenRequest(NikkiUtils.EMAIL_USER));
        accessTokenCall.enqueue(new Callback<AccessTokenResponse>()
        {
            @Override
            public void onResponse( Call<AccessTokenResponse> call, Response<AccessTokenResponse> response )
            {
                if ( response.isSuccessful() )
                {
                    AccessTokenResponse accessTokenResponse = response.body();
                    Log.i(getClass().getCanonicalName(), "Access Token: " + accessTokenResponse.getId());
                    Log.i(getClass().getCanonicalName(), "Status code: " + response.code() );
                }
                else if( response.code() == NikkiUtils.USER_CREATE_STATUS )
                {
                    Call<AccessTokenResponse> accessTokenResponseCall = loginService.sendCredentialsForLogin( new AccessTokenRequest( NikkiUtils.EMAIL_USER ) );
                    accessTokenResponseCall.enqueue(new Callback<AccessTokenResponse>()
                    {
                        @Override
                        public void onResponse(Call<AccessTokenResponse> call, Response<AccessTokenResponse> responseLogin)
                        {
                            if ( responseLogin.isSuccessful() )
                            {
                                AccessTokenResponse accessTokenResponse = responseLogin.body();
                                sessionManager.setAccessTokenResponse( accessTokenResponse );
                                Log.i(getClass().getCanonicalName(), "Access Token: " + accessTokenResponse.getId());
                                Log.i(getClass().getCanonicalName(), "Status code: " + responseLogin.code() );
                            }
                            else
                            {
                                Log.i(getClass().getCanonicalName(), "Error de servicio" );
                            }
                        }

                        @Override
                        public void onFailure(Call<AccessTokenResponse> call, Throwable t)
                        {
                            Log.i(getClass().getCanonicalName(), "Error de conexion" );
                        }
                    });
                }

                else
                {
                    Log.e(getClass().getCanonicalName(), "Error en el servicio" );
                    Log.i(getClass().getCanonicalName(), "Status code: " + response.code() );
                }
            }

            @Override
            public void onFailure(Call<AccessTokenResponse> call, Throwable t)
            {
                Log.e(getClass().getCanonicalName(), "Error de conexion");
            }
        });
    }

    @Override
    public void onCreateNote()
    {
        mView.showDialogAddNote();
    }

    @Override
    public void onCreateGroup(FragmentManager fragmentManager)
    {
        FragmentManager fm = fragmentManager;
        CreateGroupDFragment createGroupDFragment = CreateGroupDFragment.newInstance( "Crea tu Grupo" );
        createGroupDFragment.show( fm, "fragment_create_group" );
    }

    @Override
    public void setCurrentPage(int position)
    {
        currentPage = position;
    }

    @Override
    public int getCurrentPage()
    {
        return currentPage;
    }
}
