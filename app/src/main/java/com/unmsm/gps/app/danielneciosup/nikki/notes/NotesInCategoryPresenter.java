package com.unmsm.gps.app.danielneciosup.nikki.notes;

import android.content.Context;

import com.unmsm.gps.app.danielneciosup.nikki.adapter.NoteAdapter;
import com.unmsm.gps.app.danielneciosup.nikki.bean.Category;
import com.unmsm.gps.app.danielneciosup.nikki.bean.Note;
import com.unmsm.gps.app.danielneciosup.nikki.data.repository.CategoryRepository;
import com.unmsm.gps.app.danielneciosup.nikki.data.repository.NotesRepository;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.CategoriesDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.NotesDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.local.CategoryLocalDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.local.NotesLocalDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.util.NikkiUtils;
import com.unmsm.gps.app.danielneciosup.nikki.util.SessionManager;

import java.util.List;

/**
 * Created by DINJO-PC on 24/11/2016.
 */
public class NotesInCategoryPresenter implements NotesInCategoryContract.Presenter {

    private NoteAdapter noteAdapter;
    private NotesInCategoryContract.View view;
    private NotesRepository notesRepository;
    private CategoryRepository categoryRepository;
    private SessionManager sessionManager;
    private Context context;

    public NotesInCategoryPresenter(NotesInCategoryContract.View view, Context context) {
        this.context = context;
        this.view = view;
        categoryRepository = CategoryRepository.getInstance(
                null,
                CategoryLocalDataSource.getInstance(context)
        );
        notesRepository = NotesRepository.getInstance(
                null,
                NotesLocalDataSource.getInstance(context)
        );
        sessionManager = new SessionManager(context);
    }

    @Override
    public void onListNotes() {
        String categorieName = "";
        try {
            categorieName = (String) sessionManager.getSomeEntity(
                    NikkiUtils.CATEGORY_SELECTED_KEY, String.class);
        }catch (Exception ex){
            view.displaySomeMessage("Error obteniendo categoría seleccionada :(");
            return;
        }

        view.setCategoryName(categorieName);

        categoryRepository.getCategoryByName(new CategoriesDataSource.GetCategoryCallback() {
            @Override
            public void onCategoryLoaded(Category category) {
                notesRepository.getNotesByCategoryId(new NotesDataSource.LoadNotesCallback() {
                    @Override
                    public void onTaskLoaded(List<Note> notes) {
                        NoteAdapter adapter = new NoteAdapter(notes,view.getPresenter());
                        noteAdapter = adapter;
                        view.setNoteAdapter(adapter);
                    }

                    @Override
                    public void onDataNotAvailable() {
                        view.displaySomeMessage("Sin Notas");
                    }
                },category.getIdCategory());
            }

            @Override
            public void onDataNotAvailable() {
                view.displaySomeMessage("Error desconocido :(");
            }
        },categorieName);
    }

    @Override
    public void onNoteClicked(Note note) {
        view.showNoteDetail(note.getNoteId());
    }

    @Override
    public void onBackToCategoriesClick() {
        sessionManager.deleteSomeEntity(NikkiUtils.NOTE_VIEW_KEY);
        sessionManager.deleteSomeEntity(NikkiUtils.CATEGORY_SELECTED_KEY);
        view.backToCategories();
    }

    @Override
    public void onItemAdd(Note item) {
        noteAdapter.getNoteList().add(item);
        noteAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemDelete(Note item) {

    }
}
