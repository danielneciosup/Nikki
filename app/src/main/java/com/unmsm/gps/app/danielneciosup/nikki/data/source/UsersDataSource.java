package com.unmsm.gps.app.danielneciosup.nikki.data.source;

import android.support.annotation.NonNull;

import com.unmsm.gps.app.danielneciosup.nikki.bean.User;

/**
 * Created by DINJO-PC on 23/11/2016.
 */
public interface UsersDataSource {

    interface GetUserCallback{
        void onLoadedUser(User user);
        void onDataNotAvailable();
    }

    void getUserByEmail(@NonNull GetUserCallback callback,@NonNull String email );

}
