package com.unmsm.gps.app.danielneciosup.nikki.group;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.unmsm.gps.app.danielneciosup.nikki.R;

import com.unmsm.gps.app.danielneciosup.nikki.adapter.GroupsAdapter;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupResponse;

import org.json.JSONException;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Daniel on 11/09/2016.
 */

public class MyGroupsFragment extends Fragment implements MyGroupsContract.View
{
    private MyGroupsContract.Presenter presenter;

    @BindView( R.id.recview_groups )
    RecyclerView recviewGroups;

    @BindView( R.id.coordilayout_groups )
    CoordinatorLayout coordiLayoutGroups;

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState)
    {
        View myGroupsView = inflater.inflate(R.layout.fragment_my_groups, container, false);


        ButterKnife.bind( this, myGroupsView );

        setPresenter( new MyGroupsPresenter( this, getContext() ) );

        setReyclerView();

        try
        {
            presenter.onLoadGroups();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return myGroupsView;
    }

    public void setReyclerView()
    {
        recviewGroups.setLayoutManager( new LinearLayoutManager( getContext() ) );
    }

    @Override
    public void showGroups(List<GroupResponse> listGroup)
    {
        GroupsAdapter adapter = new GroupsAdapter(listGroup, new GroupsAdapter.OnGroupLongClickListener()
        {
            @Override
            public void onGroupLongClick(GroupResponse groupResponse) {
                presenter.onDeleteNote(groupResponse);
            }
        });
        recviewGroups.setAdapter(adapter);
        recviewGroups.scrollToPosition( adapter.getItemCount() - 1 );
    }

    @Override
    public void onResume()
    {
        super.onResume();
        try {
            presenter.onLoadGroups();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showMessageGroupsNotAvailable()
    {
        Snackbar.make( coordiLayoutGroups, getResources().getString( R.string.txt_groups_not_available ), Snackbar.LENGTH_SHORT ).show();
    }

    @Override
    public void showMessageGroupDelete()
    {
        Snackbar.make( coordiLayoutGroups, getResources().getString( R.string.txt_group_delete ), Snackbar.LENGTH_SHORT ).show();
    }

    @Override
    public void showMessageErrorServer()
    {

    }

    @Override
    public void showMessageErrorConnection()
    {

    }

    @Override
    public void setPresenter(MyGroupsContract.Presenter presenter)
    {
        this.presenter = presenter;
    }
}
