package com.unmsm.gps.app.danielneciosup.nikki.data.bean;

/**
 * Created by DINJO-PC on 13/11/2016.
 */
public class GroupCreateResponse
{
    private String name;
    private String id;
    private Integer quantity;

    public GroupCreateResponse(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
