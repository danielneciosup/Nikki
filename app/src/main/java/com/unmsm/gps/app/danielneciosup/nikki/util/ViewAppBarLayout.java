package com.unmsm.gps.app.danielneciosup.nikki.util;

import android.support.v4.view.ViewPager;

/**
 * Created by Daniel on 02/10/2016.
 */
public interface ViewAppBarLayout
{
    void setupToolbar();
    void setupViewPager( ViewPager viewPager );
    void setupTabLayout();
}
