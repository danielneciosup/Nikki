package com.unmsm.gps.app.danielneciosup.nikki.notes;

import com.unmsm.gps.app.danielneciosup.nikki.adapter.CategoryAdapter;
import com.unmsm.gps.app.danielneciosup.nikki.bean.Category;
import com.unmsm.gps.app.danielneciosup.nikki.bean.Note;
import com.unmsm.gps.app.danielneciosup.nikki.util.BaseView;
import com.unmsm.gps.app.danielneciosup.nikki.util.RefreshableContract;

/**
 * Created by Daniel on 16/10/2016.
 */
public interface MyNotesContract
{
    public interface Presenter extends RefreshableContract.Presenter<Note>
    {
        void onCategorySelected(Category category);
        void onListCategories();
    }

    public interface View extends BaseView<Presenter>
    {
        void showListNotes();
        void setRecyclerView();
        void setCategoryAdapter(CategoryAdapter adapter);
        void showNotesInCategorySelected();
        void displaySomeMessage(String message);
        Presenter getPresenter();
        void setParentView(MainNotesContract.View view);
    }
}
