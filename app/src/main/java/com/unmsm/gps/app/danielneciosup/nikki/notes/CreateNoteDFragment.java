package com.unmsm.gps.app.danielneciosup.nikki.notes;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;

import com.unmsm.gps.app.danielneciosup.nikki.R;
import com.unmsm.gps.app.danielneciosup.nikki.home.HomeContract;
import com.unmsm.gps.app.danielneciosup.nikki.util.NikkiUtils;
import com.unmsm.gps.app.danielneciosup.nikki.util.RefreshableContract;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnFocusChange;
import butterknife.Unbinder;

/**
 * Created by Daniel on 02/11/2016.
 */
public class CreateNoteDFragment extends DialogFragment implements CreateNoteContract.View
{
    private Unbinder unbinder;
    private CreateNoteContract.Presenter mPresenter;
    private RefreshableContract.Presenter refresheableNotePresenter;
    private MainNotesContract.Presenter forNotificationsPresenter;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;

    @BindView(R.id.editxt_new_note)EditText NoteTitle;
    @BindView(R.id.editxt_date_note)EditText NoteDate;
    @BindView(R.id.editxt_hour_note)EditText NoteTime;

    public static CreateNoteDFragment newInstance()
    {
        return new CreateNoteDFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View mView = inflater.inflate(R.layout.dfragment_create_note, container, false );

        getDialog().setTitle( getString( R.string.txt_txtview_create_note ) );

        setPresenter( new CreateNotePresenter(this,getContext()) );

        datePickerDialog = new DatePickerDialog(getContext(),mPresenter,
                NikkiUtils.SHARED_CALENDAR.get(NikkiUtils.SHARED_CALENDAR.YEAR),
                NikkiUtils.SHARED_CALENDAR.get(NikkiUtils.SHARED_CALENDAR.MONTH),
                NikkiUtils.SHARED_CALENDAR.get(NikkiUtils.SHARED_CALENDAR.DAY_OF_MONTH));

        timePickerDialog = new TimePickerDialog(getContext(),mPresenter,
                NikkiUtils.SHARED_CALENDAR.get(NikkiUtils.SHARED_CALENDAR.HOUR),
                NikkiUtils.SHARED_CALENDAR.get(NikkiUtils.SHARED_CALENDAR.MINUTE),false);

        return mView;
    }

    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState )
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind( this, view );
    }

    @OnClick(R.id.btn_create_note)
    public void display(){
        mPresenter.onCreate();
    }

    @Override
    @OnClick( R.id.btn_cancel_create_note )
    public void displayCancel()
    {
        mPresenter.onCancel(getDialog());
    }

    @OnClick(R.id.editxt_date_note)
    public void onDateEdit(){
        mPresenter.onDateEdit();
    }

    @OnClick(R.id.editxt_hour_note)
    public void OnTimeEdit(){
        mPresenter.onTimeEdit();
    }

    @Override
    public void showDatePicker()
    {
        datePickerDialog.show();
    }

    @Override
    public void showTimePicker() {
        timePickerDialog.show();
    }

    @Override
    public void setSelectedDate(String date) {
        NoteDate.setText(date);
    }

    @Override
    public void setSelectedTime(String time) {
        NoteTime.setText(time);
    }

    @Override
    public String getNoteTitle() {
        return NoteTitle.getText().toString();
    }

    @Override
    public String getNoteDate() {
        return NoteDate.getText().toString();
    }

    @Override
    public String getNoteTime() {
        return NoteTime.getText().toString();
    }

    @Override
    public void displaySomeMessage(String errorMessage) {
        Snackbar snackbar = Snackbar.make(getView(),errorMessage,Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    public void setPresenter(CreateNoteContract.Presenter presenter)
    {
        mPresenter = presenter;
    }


    @Override
    public void displayNoteCreated() {
        /*Snackbar snackbar = Snackbar.make(parentView,"Nota creada con éxito",Snackbar.LENGTH_SHORT);
        snackbar.show();*/
        forNotificationsPresenter.showNotification("Nota creada con éxito");
    }


    @Override
    public void hide() {
        getDialog().dismiss();
    }

    @Override
    public void setRefresheableNotePresenter(RefreshableContract.Presenter presenter) {
        this.refresheableNotePresenter = presenter;
    }

    @Override
    public RefreshableContract.Presenter getRefresheableNotePresenter() {
        return refresheableNotePresenter;
    }

    @Override
    public void setParentPresenter(MainNotesContract.Presenter presenter) {
        forNotificationsPresenter = presenter;
    }

    @Override
    public MainNotesContract.Presenter getParentPresenter() {
        return forNotificationsPresenter;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
