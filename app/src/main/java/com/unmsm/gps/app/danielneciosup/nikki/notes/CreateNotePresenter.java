package com.unmsm.gps.app.danielneciosup.nikki.notes;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.unmsm.gps.app.danielneciosup.nikki.bean.Category;
import com.unmsm.gps.app.danielneciosup.nikki.bean.Note;
import com.unmsm.gps.app.danielneciosup.nikki.bean.User;
import com.unmsm.gps.app.danielneciosup.nikki.data.repository.CategoryRepository;
import com.unmsm.gps.app.danielneciosup.nikki.data.repository.NotesRepository;
import com.unmsm.gps.app.danielneciosup.nikki.data.repository.UsersRepository;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.CategoriesDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.NotesDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.UsersDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.local.CategoryLocalDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.local.NotesLocalDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.local.UsersLocalDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.util.NikkiUtils;
import com.unmsm.gps.app.danielneciosup.nikki.util.SessionManager;

import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Daniel on 16/10/2016.
 */
public class CreateNotePresenter implements CreateNoteContract.Presenter
{
    private CreateNoteContract.View mView;

    private UsersRepository usersRepository;

    private NotesRepository notesRepository;

    private CategoryRepository categoryRepository;

    private SessionManager sessionManager;


    public CreateNotePresenter(CreateNoteContract.View view, Context context)
    {
        this.mView = view;

        notesRepository = NotesRepository.getInstance(
                null,
                NotesLocalDataSource.getInstance(context)
        );
        usersRepository = UsersRepository.getInstance(
                null,
                UsersLocalDataSource.getInstance(context)
        );
        categoryRepository = CategoryRepository.getInstance(
                null,
                CategoryLocalDataSource.getInstance(context)
        );
        sessionManager = new SessionManager( context );
    }

    @Override
    public void onCreate() {

        final Category[] noteCategory = new Category[1];

        categoryRepository.getCategoryByNoteTitle(new CategoriesDataSource.GetCategoryCallback() {
            @Override
            public void onCategoryLoaded(final Category category) {
                noteCategory[0] = category;
            }

            @Override
            public void onDataNotAvailable() {
                mView.displaySomeMessage("Algún error :(");
            }
        },mView.getNoteTitle());

        final Note note = new Note(){{
            setTxDescrip(mView.getNoteTitle());
            try {
                setDtRun((new SimpleDateFormat("dd-MM-yyyy hh:mm:ss"))
                        .parse(mView.getNoteDate()+" "+mView.getNoteTime()) );
            } catch (ParseException e) {
                mView.displaySomeMessage("Ha ingresado correctamente la fecha y hora?");
            }
            usersRepository.getUserByEmail(new UsersDataSource.GetUserCallback() {
                @Override
                public void onLoadedUser(User user) {
                    setUser(user);
                }

                @Override
                public void onDataNotAvailable() {
                    mView.displaySomeMessage("Usuario no existe");
                }
            }, NikkiUtils.EMAIL_USER);
            setCategory(noteCategory[0]);
        }};

        notesRepository.createNote(new NotesDataSource.InsertNoteCallback() {
            @Override
            public void onNoteInserted() {
                mView.displayNoteCreated();
                String actualCategory = null;
                try {
                    actualCategory = (String)sessionManager.getSomeEntity(NikkiUtils.CATEGORY_SELECTED_KEY,String.class);
                } catch (JSONException e) {
                    mView.displaySomeMessage("Error actualizando vista de notas :(");
                }
                if( actualCategory != null && actualCategory.equals(note.getCategory().getTxtNombre()) ){
                    mView.getRefresheableNotePresenter().onItemAdd(note);
                }
                mView.hide();
            }

            @Override
            public void onNoteNotInserted() {
                mView.displaySomeMessage("La nota no pudo ser creada");
            }

        },note);
    }

    @Override
    public void onCancel(Dialog dialog)
    {
        dialog.dismiss();
    }

    @Override
    public void onDateEdit() {
        mView.showDatePicker();
    }

    @Override
    public void onTimeEdit() {
        mView.showTimePicker();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        String dateSelected = String.format("%02d-%02d-%d",i2,i1,i);
        mView.setSelectedDate(dateSelected);
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int i, int i1) {
        String timeSelected = String.format("%02d:%02d:00",i,i1);
        mView.setSelectedTime(timeSelected);
    }
}
