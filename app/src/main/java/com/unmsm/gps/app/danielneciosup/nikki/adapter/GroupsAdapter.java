package com.unmsm.gps.app.danielneciosup.nikki.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.unmsm.gps.app.danielneciosup.nikki.R;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Daniel on 25/11/2016.
 */
public class GroupsAdapter extends RecyclerView.Adapter<GroupsAdapter.GroupViewHolder>
{
    public interface OnGroupLongClickListener
    {
        void onGroupLongClick(GroupResponse groupResponse);
    }

    private List<GroupResponse> groups;

    private OnGroupLongClickListener listener;

    public GroupsAdapter(List<GroupResponse> groups, OnGroupLongClickListener listener)
    {
        this.groups = groups;
        this.listener = listener;
    }

    public class GroupViewHolder extends RecyclerView.ViewHolder
    {
        @BindView( R.id.txtview_nom_group_for_card )
        TextView txtviewNomGroupForCard;

        @BindView( R.id.imgview_crown )
        ImageView imgviewCrown;

        @BindView( R.id.txtview_num_integrantes )
        TextView txtviewNumIntegrantes;

        public GroupViewHolder(View itemView)
        {
            super(itemView);
            ButterKnife.bind( this, itemView );
        }

        public void bind(final GroupResponse groupResponse, final OnGroupLongClickListener listener )
        {
            txtviewNomGroupForCard.setText( groupResponse.getName() );
            txtviewNumIntegrantes.setText( String.format(String.valueOf(groupResponse.getQuantity())) );
            if ( ! groupResponse.getIsadmin() )
                imgviewCrown.setVisibility( ImageView.INVISIBLE );
            itemView.setOnLongClickListener(new View.OnLongClickListener()
            {
                @Override
                public boolean onLongClick(View view)
                {
                    listener.onGroupLongClick( groupResponse );

                    return true;
                }
            });
        }
    }

    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.carview_group, parent, false );

        return new GroupViewHolder( view );
    }

    @Override
    public void onBindViewHolder(GroupViewHolder holder, int position)
    {
        holder.bind( groups.get( position ), listener );
    }

    @Override
    public int getItemCount()
    {
        return groups.size();
    }

    public void setListener(OnGroupLongClickListener listener)
    {
        this.listener = listener;
    }
}
