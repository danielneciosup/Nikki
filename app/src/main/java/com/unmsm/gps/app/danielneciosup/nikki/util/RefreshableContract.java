package com.unmsm.gps.app.danielneciosup.nikki.util;

/**
 * Created by DINJO-PC on 24/11/2016.
 */
public interface RefreshableContract {

    public interface View{
    }

    public interface Presenter<T>{
        void onItemAdd(T item);
        void onItemDelete(T item);
    }

}


