package com.unmsm.gps.app.danielneciosup.nikki.data.source.remote.service;

import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateResponse;
<<<<<<< HEAD
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
=======
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupResponse;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
>>>>>>> 3e23c0fe905f523d81d4cadbfa5de270462ebf52

/**
 * Created by DINJO-PC on 13/11/2016.
 */
public interface GroupService
{
    @POST( "users/create/group" )
    Call<GroupCreateResponse> createGroup(@Body GroupCreateRequest request);

    @POST( "users/added/users" )
    Call<UserAddedToGroupResponse> addUserToGroup(@Body UserAddedToGroupRequest request);
<<<<<<< HEAD
=======

    @GET( "users/list/groups" )
    Call<List<GroupResponse>> getGroupsByUser( @Query( "userid" ) String userid  );
>>>>>>> 3e23c0fe905f523d81d4cadbfa5de270462ebf52
}
