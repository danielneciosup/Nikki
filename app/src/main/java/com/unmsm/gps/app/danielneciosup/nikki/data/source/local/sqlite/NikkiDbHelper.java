package com.unmsm.gps.app.danielneciosup.nikki.data.source.local.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.unmsm.gps.app.danielneciosup.nikki.bean.Category;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.local.CategoryLocalDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.util.NikkiUtils;

/**
 * Created by Daniel on 21/10/2016.
 */
public class NikkiDbHelper extends SQLiteOpenHelper
{
    /**
     * Version of Database
     */
    public static final int DATABASE_VERSION = 5;

    /**
     * Name of database
     */
    public static final String DATABASE_NAME = "Nikki.db";

    /**
     * Strings of the create tables
     */

    private static final String SQL_CREATE_CATEGORY =
        "CREATE TABLE " + NikkiDbContract.CategoryEntry.TABLE_NAME +
        "(" +
            NikkiDbContract.CategoryEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            NikkiDbContract.CategoryEntry.TX_NAME + " TEXT NOT NULL" +
            //NikkiDbContract.CategoryEntry.CATEGORY_FATHER_ID + " INTEGER," +

            //"FOREIGN KEY (" + NikkiDbContract.CategoryEntry.CATEGORY_FATHER_ID + ") REFERENCES " + NikkiDbContract.CategoryEntry.TABLE_NAME + "(" + NikkiDbContract.CategoryEntry._ID + ")" +
        ");";

    private static final String SQL_CREATE_GROUP =
        "CREATE TABLE " + NikkiDbContract.GroupEntry.TABLE_NAME +
        "(" +
            NikkiDbContract.GroupEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            NikkiDbContract.GroupEntry.TX_NAME + " TEXT NOT NULL," +
            NikkiDbContract.GroupEntry.DT_CREATE + " DATETIME" +
        ");";

    private static final String SQL_CREATE_USER =
        "CREATE TABLE " + NikkiDbContract.UserEntry.TABLE_NAME +
        "(" +
            NikkiDbContract.UserEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            NikkiDbContract.UserEntry.TX_EMAIL + " TEXT NOT NULL" +
        ");";

    private static final String SQL_CREATE_ALARM =
        "CREATE TABLE " + NikkiDbContract.AlarmEntry.TABLE_NAME +
        "(" +
            NikkiDbContract.AlarmEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            NikkiDbContract.AlarmEntry.HR_ALARM + " TIME," +
            NikkiDbContract.AlarmEntry.NOTE_ID + " INTEGER," +

            "FOREIGN KEY (" + NikkiDbContract.AlarmEntry.NOTE_ID + ") REFERENCES " + NikkiDbContract.NoteEntry.TABLE_NAME + "(" + NikkiDbContract.NoteEntry._ID + ")" +
        ");";

    private static final String SQL_CREATE_NOTE =
        "CREATE TABLE " + NikkiDbContract.NoteEntry.TABLE_NAME +
        "(" +
            NikkiDbContract.NoteEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            NikkiDbContract.NoteEntry.TX_DESCRIP + " TEXT NOT NULL," +
            NikkiDbContract.NoteEntry.DT_CREATE + " DATETIME DEFAULT CURRENT_TIMESTAMP," +
            NikkiDbContract.NoteEntry.DT_RUN + " DATETIME NOT NULL," +
            NikkiDbContract.NoteEntry.USER_ID + " INTEGER," +
            NikkiDbContract.NoteEntry.CATEGORY_ID + " INTEGER," +
            NikkiDbContract.NoteEntry.GROUP_ID + " INTEGER," +

            "FOREIGN KEY (" + NikkiDbContract.NoteEntry.USER_ID + ") REFERENCES " + NikkiDbContract.UserEntry.TABLE_NAME +  "(" + NikkiDbContract.UserEntry._ID + ")," +
            "FOREIGN KEY (" + NikkiDbContract.NoteEntry.CATEGORY_ID + ") REFERENCES " + NikkiDbContract.CategoryEntry.TABLE_NAME +  "(" + NikkiDbContract.CategoryEntry._ID + ")," +
            "FOREIGN KEY (" + NikkiDbContract.NoteEntry.GROUP_ID + ") REFERENCES " + NikkiDbContract.GroupEntry.TABLE_NAME +  "(" + NikkiDbContract.GroupEntry._ID + ")" +
        ");";

    private static final String SQL_CREATE_USERGROUP =
        "CREATE TABLE " + NikkiDbContract.UserGroupEntry.TABLE_NAME +
        "(" +
            NikkiDbContract.UserGroupEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            NikkiDbContract.UserGroupEntry.USER_ID + " INTEGER NOT NULL," +
            NikkiDbContract.UserGroupEntry.GROUP_ID + " INTEGER NOT NULL," +
            NikkiDbContract.UserGroupEntry.BL_ISADMIN + " BOOLEAN," +

            "FOREIGN KEY (" + NikkiDbContract.UserGroupEntry.USER_ID + ") REFERENCES " + NikkiDbContract.UserEntry.TABLE_NAME +  "(" + NikkiDbContract.UserEntry._ID + ")," +
            "FOREIGN KEY (" + NikkiDbContract.UserGroupEntry.GROUP_ID + ") REFERENCES " + NikkiDbContract.GroupEntry.TABLE_NAME +  "(" + NikkiDbContract.GroupEntry._ID + ")" +
        ");";

    /**
     * INSTANCE unique
     */
    private static NikkiDbHelper INSTANCE;

    /**
     * Using for Singleton Pattern
     *
     * @param context actual of application
     * @return unique instance of database
     */
    public static NikkiDbHelper getInstance( Context context )
    {
        if ( INSTANCE == null )
           INSTANCE = new NikkiDbHelper( context );

        return INSTANCE;
    }

    private NikkiDbHelper( Context context )
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION );
    }

    private void initializeCategories(SQLiteDatabase sqLiteDatabase){
        String[] categories = new String[]{
            NikkiUtils.DEFAULT_CATEGORY,"Tarea","Examen","Proyecto","Presentación"
        };

        for(String category : categories){
            sqLiteDatabase.execSQL(
                    NikkiDbContract.CategoryEntry.COMMON_INSERT_COMMAND,
                    new String[] { category }
            );
        }
    }
    private void initializeUsers(SQLiteDatabase sqLiteDatabase){
        String[] usersDefault = new String[]{
                NikkiUtils.EMAIL_USER
        };

        for(String user : usersDefault){
            sqLiteDatabase.execSQL(
                    NikkiDbContract.UserEntry.COMMON_INSERT_COMMAND,
                    new String[] { user }
            );
        }
    }


    @Override
    public void onCreate( SQLiteDatabase sqLiteDatabase )
    {
        sqLiteDatabase.execSQL(SQL_CREATE_CATEGORY);
        sqLiteDatabase.execSQL(SQL_CREATE_GROUP);
        sqLiteDatabase.execSQL(SQL_CREATE_USER);
        sqLiteDatabase.execSQL(SQL_CREATE_NOTE);
        sqLiteDatabase.execSQL(SQL_CREATE_ALARM);
        sqLiteDatabase.execSQL(SQL_CREATE_USERGROUP);
        initializeCategories(sqLiteDatabase);
        initializeUsers(sqLiteDatabase);
    }

    @Override
    public void onUpgrade( SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion )
    {
        // Not required as at version 1
        if( oldVersion != newVersion )
        {
            sqLiteDatabase.execSQL( "DROP TABLE IF EXISTS " + NikkiDbContract.CategoryEntry.TABLE_NAME );
            sqLiteDatabase.execSQL( "DROP TABLE IF EXISTS " + NikkiDbContract.GroupEntry.TABLE_NAME );
            sqLiteDatabase.execSQL( "DROP TABLE IF EXISTS " + NikkiDbContract.UserEntry.TABLE_NAME );
            sqLiteDatabase.execSQL( "DROP TABLE IF EXISTS " + NikkiDbContract.AlarmEntry.TABLE_NAME );
            sqLiteDatabase.execSQL( "DROP TABLE IF EXISTS " + NikkiDbContract.NoteEntry.TABLE_NAME );
            sqLiteDatabase.execSQL( "DROP TABLE IF EXISTS " + NikkiDbContract.UserGroupEntry.TABLE_NAME );
            onCreate( sqLiteDatabase );
        }
    }
}
