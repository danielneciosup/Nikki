package com.unmsm.gps.app.danielneciosup.nikki.data.repository;

import android.support.annotation.NonNull;

import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateResponse;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupResponse;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.GroupsDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.local.GroupLocalDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.remote.GroupRemoteDataSource;

import java.util.List;
/**
 * Created by DINJO-PC on 19/11/2016.
 */
public class GroupRepository implements GroupsDataSource
{
    private static GroupRepository instance = null;
    private final GroupLocalDataSource  groupLocalDataSource;
    private final GroupRemoteDataSource groupRemoteDataSource;

    /**
     *
     * @param groupLocalDataSource source local of groups.
     * @param groupRemoteDataSource source remote of groups.
     */
    private GroupRepository(@NonNull GroupLocalDataSource groupLocalDataSource,
                           @NonNull GroupRemoteDataSource groupRemoteDataSource)
    {
        this.groupLocalDataSource = groupLocalDataSource;
        this.groupRemoteDataSource = groupRemoteDataSource;
    }

    public static GroupRepository getInstance(@NonNull GroupLocalDataSource groupLocalDataSource,
                                              @NonNull GroupRemoteDataSource groupRemoteDataSource)
    {
        if ( instance == null )
        {
            instance = new GroupRepository( groupLocalDataSource, groupRemoteDataSource );
        }

        return instance;
    }

    public void getGroups(@NonNull final LoadedGroupsCallback callback, @NonNull String userId)
    {
        groupRemoteDataSource.getGroups(new LoadedGroupsCallback()
        {
            @Override
            public void onGroupsLoaded(List<GroupResponse> groups)
            {
                callback.onGroupsLoaded( groups );
            }

            @Override
            public void onDataNotAvailable()
            {
                callback.onDataNotAvailable();
            }

            @Override
            public void onErrorServer()
            {
                callback.onErrorServer();
            }

            @Override
            public void onErrorConnection()
            {
                callback.onErrorConnection();
            }
        }, userId);
    }

    @Override
    public void createGroup(@NonNull final LoadedGroupCreateCallback callback, @NonNull GroupCreateRequest request)
    {
        /*
        groupLocalDataSource.createGroup(new LoadedGroupCreateCallback()
        {
            @Override
            public void onGroupCreateLoaded(GroupCreateResponse groupCreateResponse)
            {
                callback.onGroupCreateLoaded( groupCreateResponse );
            }

            @Override
            public void onErrorService()
            {
                callback.onErrorService();
            }

            @Override
            public void onErrorConnection()
            {
                callback.onErrorConnection();
            }
        }, request);
        */
        groupRemoteDataSource.createGroup(new LoadedGroupCreateCallback()
        {
            @Override
            public void onGroupCreateLoaded(GroupCreateResponse groupCreateResponse)
            {
                callback.onGroupCreateLoaded( groupCreateResponse );
            }

            @Override
            public void onErrorService()
            {
                callback.onErrorService();
            }

            @Override
            public void onErrorConnection()
            {
                callback.onErrorConnection();
            }
        }, request);
    }

    @Override
    public void getUserAddedToGroup(@NonNull LoadedUserAddedCallback callback, @NonNull UserAddedToGroupRequest request)
    {
        groupRemoteDataSource.getUserAddedToGroup( callback, request );
    }
}
