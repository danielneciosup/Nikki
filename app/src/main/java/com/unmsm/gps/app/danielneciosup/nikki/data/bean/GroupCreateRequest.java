package com.unmsm.gps.app.danielneciosup.nikki.data.bean;

/**
 * Created by DINJO-PC on 13/11/2016.
 */
public class GroupCreateRequest
{
    private String name;
    private String userid;

    public GroupCreateRequest(String name, String userid)
    {
        this.name = name;
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
