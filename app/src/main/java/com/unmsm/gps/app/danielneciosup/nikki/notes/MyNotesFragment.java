package com.unmsm.gps.app.danielneciosup.nikki.notes;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.unmsm.gps.app.danielneciosup.nikki.R;
import com.unmsm.gps.app.danielneciosup.nikki.adapter.CategoryAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Daniel on 11/09/2016.
 */
public class MyNotesFragment extends Fragment implements MyNotesContract.View
{
    MyNotesContract.Presenter presenter;
    private MainNotesContract.View parentView;
    private RecyclerView.Adapter mAdapter;

    @BindView( R.id.recview_categories )
    public RecyclerView recViewCategories;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View mViewNotes = inflater.inflate( R.layout.fragment_my_notes, container, false );

        ButterKnife.bind(this, mViewNotes);

        setPresenter(new MyNotesPresenter(this, getContext()));

        setRecyclerView();

        presenter.onListCategories();

        return mViewNotes;
    }

    @Override
    public void showListNotes()
    {

    }

    @Override
    public void setRecyclerView()
    {
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager( getContext(), 3 );
        recViewCategories.setLayoutManager( layoutManager );
    }

    @Override
    public void setCategoryAdapter(CategoryAdapter adapter) {
        recViewCategories.setAdapter(adapter);
    }

    @Override
    public void showNotesInCategorySelected() {
        //Snackbar snackbar = Snackbar.make(getView(),category,Snackbar.LENGTH_SHORT);
        //snackbar.show();
        parentView.showNotesView();

    }

    @Override
    public void displaySomeMessage(String message) {
        Snackbar snackbar = Snackbar.make(getView(),message,Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    public MyNotesContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void setParentView(MainNotesContract.View parentView) {
        this.parentView = parentView;
    }

    @Override
    public void setPresenter(MyNotesContract.Presenter presenter)
    {
        this.presenter = presenter;
    }

}
