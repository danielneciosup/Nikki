package com.unmsm.gps.app.danielneciosup.nikki.data.bean;


import java.util.Date;

/**
 * Created by DINJO-PC on 12/11/2016.
 */
public class AccessTokenResponse
{
    private String id;
    private int ttl;
    private Date created;
    private String userid;

    public AccessTokenResponse(String id, int ttl, Date created, String userid) {
        this.id = id;
        this.ttl = ttl;
        this.created = created;
        this.userid = userid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getTtl() {
        return ttl;
    }

    public void setTtl(int ttl) {
        this.ttl = ttl;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
