package com.unmsm.gps.app.danielneciosup.nikki.notes;

import android.content.Context;

import com.unmsm.gps.app.danielneciosup.nikki.util.NikkiUtils;
import com.unmsm.gps.app.danielneciosup.nikki.util.SessionManager;

/**
 * Created by DINJO-PC on 24/11/2016.
 */
public class MainNotesPresenter implements MainNotesContract.Presenter{

    private MainNotesContract.View view;

    private SessionManager sessionManager;

    public MainNotesPresenter(MainNotesContract.View view, Context context) {
        sessionManager = new SessionManager(context);
        this.view = view;
    }

    @Override
    public void onViewToShowRequired() {
        String viewName = NikkiUtils.NOTES_DEFAULT_VIEW;
        try{
            String viewNameFromSession = (String)sessionManager.getSomeEntity(NikkiUtils.NOTE_VIEW_KEY,String.class);
            viewName = (viewNameFromSession == null) ? viewName : viewNameFromSession;
        }catch (Exception ex){

        }
        if( viewName.equals(MyNotesFragment.class.getCanonicalName()) ){
            view.showCategoriesView();
        }else if(viewName.equals(NotesInCategoryFragment.class.getCanonicalName())){
            view.showNotesView();
        }else{
            view.showSomeMessage("Error desconocido");
        }
    }

    @Override
    public void showNotification(String message) {
        view.showSomeMessage(message);
    }
}
