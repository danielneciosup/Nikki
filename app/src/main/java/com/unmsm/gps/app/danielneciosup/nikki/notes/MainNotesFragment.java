package com.unmsm.gps.app.danielneciosup.nikki.notes;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.unmsm.gps.app.danielneciosup.nikki.R;
import com.unmsm.gps.app.danielneciosup.nikki.util.BaseView;
import com.unmsm.gps.app.danielneciosup.nikki.util.NikkiUtils;
import com.unmsm.gps.app.danielneciosup.nikki.util.RefreshableContract;
import com.unmsm.gps.app.danielneciosup.nikki.util.SessionManager;

import butterknife.ButterKnife;

/**
 * Created by DINJO-PC on 24/11/2016.
 */
public class MainNotesFragment extends Fragment implements MainNotesContract.View{

    RefreshableContract.Presenter refresheablePresenterOfActiveView;
    MainNotesContract.Presenter presenter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View mView = inflater.inflate( R.layout.fragment_main_notes, container, false );

        setPresenter(new MainNotesPresenter(this,getContext()));

        presenter.onViewToShowRequired();

        return mView;
    }

    @Override
    public void showCategoriesView() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        MyNotesFragment fragment = new MyNotesFragment();
        refresheablePresenterOfActiveView = fragment.getPresenter();
        fragment.setParentView(this);
        ft.replace(R.id.notes_container,fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void showNotesView() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        NotesInCategoryFragment fragment = new NotesInCategoryFragment();
        refresheablePresenterOfActiveView = fragment.getPresenter();
        ft.replace(R.id.notes_container,fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();
        fragment.setParentView(this);
    }

    @Override
    public void showSomeMessage(String message) {
        Snackbar snackbar = Snackbar.make(getView(),message,Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    public MainNotesContract.Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void setRefresheablePresenterOfActiveView(RefreshableContract.Presenter presenter) {
        this.refresheablePresenterOfActiveView = presenter;
    }

    @Override
    public RefreshableContract.Presenter getRefresheablePresenterOfActiveView() {
        return refresheablePresenterOfActiveView;
    }


    @Override
    public void setPresenter(MainNotesContract.Presenter presenter) {
        this.presenter = presenter;
    }
}
